#include "protocol.h"

using namespace std;

namespace hwo_protocol
{
	jsoncons::json make_request(const string &msg_type, const jsoncons::json &data)
	{
		jsoncons::json r;
		r["msgType"] = msg_type;
		r["data"] = data;
		return r;
	}

	jsoncons::json make_join(const string &name, const string &key)
	{
		jsoncons::json data;
		data["name"] = name;
		data["key"]  = key;
		return make_request("join", data);
	}
	
	jsoncons::json make_joinRace(const string &name, const string &key, const char *track, const char *password, int cars)
	{
		jsoncons::json data, botid;
		
		botid["name"] = name;
		botid["key"]  = key;
		data["botId"] = botid;
		
		if (track != nullptr)
			data["trackName"] = string(track);
		
		if (password != nullptr)
			data["password"] = string(track);
		
		data["carCount"] = cars;
		return make_request("joinRace", data);
	}

	jsoncons::json make_ping()
	{
		return make_request("ping", jsoncons::null_type());
	}

	jsoncons::json make_throttle(double throttle)
	{
		return make_request("throttle", throttle);
	}

}  // namespace hwo_protocol
