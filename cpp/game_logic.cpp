#include "game_logic.h"
#include "protocol.h"

using namespace std;
using namespace jsoncons;
using namespace hwo_protocol;

#define MAX_ANGLE			60.0
#define MAX_VELOCITY		6.0		// ?

game_logic::game_logic() : mGameTick(0), mLastPiece(0), mLastThrottle(1.0), mMinAngle(0.0), mMaxAngle(0.0), mEstDist(0.0), mActDist(0.0),
	action_map
	{
		{ "join",			&game_logic::on_join			},
		{ "joinRace",		&game_logic::on_join			},
		{ "yourCar",		&game_logic::on_your_car		},
		{ "gameInit",		&game_logic::on_game_init		},
		{ "gameStart",		&game_logic::on_game_start		},
		{ "carPositions",	&game_logic::on_car_positions	},
		{ "crash",			&game_logic::on_crash			},
		{ "gameEnd",		&game_logic::on_game_end		},
		{ "error",			&game_logic::on_error			}
    }
{
	// store a single empty angle in our history buffer
	mAngleHistory.push_back(0.0);
}

double game_logic::getLaneOffset(int lane) const
{
	if ((lane < 0) || (lane > mLaneDist.size()))
	{
		cout << "*** request for invalid lane offset, lane=" << lane << endl;
		return 0.0;
	}
	
	return mLaneDist[lane];
}

double game_logic::calculateThrottle(int idx) const
{
	const TrackPiece &piece = mTrackPiece[idx];
	const TrackPiece &next  = mTrackPiece[idx + 1];
	
	// if this is a straight piece, but the next is a bend of 45 degrees or more, kill our throttle
	if ((piece.type == TypeStraight) && (next.type == TypeBend) && (abs(next.angle) >= 45.0))
		return 0.0;
	
	// if this is a bend of 45 degrees or more, slow down
	if ((piece.type == TypeBend) && (abs(piece.angle) >= 45.0))
		return 0.5;
	
	// full throttle, baybee!
	return 1.0;
}

double game_logic::calculateBendLength(int piece, double offset) const
{
	const double radius = mTrackPiece[piece].radius + offset;
	const double circum = 2.0 * M_PI * radius;
	return (circum * abs(mTrackPiece[piece].angle) / 360.0);
}

game_logic::msg_vector game_logic::react(const json &msg)
{
	const auto &msg_type = msg["msgType"].as<string>();
	const auto &data = msg["data"];
	auto action_it = action_map.find(msg_type);
	
	// get and store game tick if it exists in the message
	if (msg.has_member("gameTick") == true)
		mGameTick = msg["gameTick"].as<int>();

	if (action_it != action_map.end())
	{
		return (action_it->second)(this, data);
	}
	else
	{
		cout << "Unknown message type: " << msg_type << endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join(const json &data)
{
	cout << "Joined" << endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const json &data)
{
	const auto &name = data["name"].as<string>();

	// store our car color
	mMyCarColor = data["color"].as<string>();
	cout << "My car: name=" << name << ", color=" << mMyCarColor << endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const json &data)
{
	int t;
	
	// load lane distances first
	const auto &lanes = data["race"]["track"]["lanes"];
	const int laneCount = lanes.size();

	// probably not necessary, but let's guard against a response of zero lanes
	if (laneCount > 0)
	{
		mLaneDist.resize(laneCount, 0.0);
		
		for (t = 0; t < laneCount; t++)
		{
			const auto &lane = lanes[t];
			const double dist = lane["distanceFromCenter"].as<double>();
			const int index = lane["index"].as<int>();
	
			// discard invalid indices
			if (index > laneCount)
			{
				cout << "*** got invalid lane index of [" << index << "], max lanes=[" << laneCount << "]" << endl;
				continue;
			}
			
			mLaneDist[index] = dist;
		}
	}
	else
	{
		// just make one lane
		mLaneDist.push_back(0.0);
	}
	
	// load track data
	const auto &pieces = data["race"]["track"]["pieces"];
	for (t = 0; t < pieces.size(); t++)
	{
		const auto &track = pieces[t];
		TrackPiece piece;

		piece.brake = false;
		piece.hasSwitch = false;
		if (track.has_member("switch") == true)
			piece.hasSwitch = track["switch"].as<bool>();
		
		// determine piece type
		if (track.has_member("radius") == false)
		{
			piece.type	 = TypeStraight;
			piece.length = track["length"].as<double>();
			piece.radius = 0.0;
			piece.angle	 = 0.0;
		}
		else
		{
			piece.type	 = TypeBend;
			piece.radius = track["radius"].as<double>();
			piece.angle	 = track["angle"].as<double>();
		}
		
		// add piece to our track data
		mTrackPiece.push_back(piece);
	}
	
	// calculate/dump track pieces
	for (t = 0; t < mTrackPiece.size(); t++)
	{
		TrackPiece &piece = mTrackPiece[t];
		TrackPiece &next  = mTrackPiece[t + 1];
		
		// calculate length of bend piece from midpoint
		if (piece.type == TypeBend)
			piece.length = calculateBendLength(t, 0.0);
		
		// if next piece is a bend with a high angle and low radius, then apply brake on this piece
		if ((next.type == TypeBend) && (abs(next.angle) >= 30.0) && (next.radius <= 150.0))
			piece.brake = true;
				
//		piece.throttle = calculateThrottle(t);
				
		// dump piece info
		if (piece.type == TypeStraight)
			cout << "piece #" << t << " (straight): length=" << piece.length << ", switch=" << piece.hasSwitch << ", brake=" << piece.brake << endl;
		else
			cout << "piece #" << t << " (bend): length=" << piece.length << ", radius=" << piece.radius << ", angle=" << piece.angle << ", switch=" << piece.hasSwitch << ", brake=" << piece.brake << endl;
	}
	
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const json &data)
{
	cout << "Race started" << endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const json &data)
{
	double throttle, angle = 0.0, inDist = 0.0;
	bool found = false;
	
	// iterate through all cars
	for (int t = 0; t < data.size(); t++)
	{
		const auto &car   = data[t];
		const auto &color = car["id"]["color"].as<string>();
		
		// if not our car, skip
		if (mMyCarColor != color)
			continue;
		
		// get angle and current piece index
		angle = car["angle"].as<double>();
		mCurPiece = car["piecePosition"]["pieceIndex"].as<int>();
		inDist = car["piecePosition"]["inPieceDistance"].as<double>();
		found = true;
		break;
	}
	
	if (found == true)
	{
		const TrackPiece &track = mTrackPiece[mCurPiece];
		
		// tick 1800: angle=0.0468201, min=-31.1434, max=5.63959	<- throttle = 0.6
		// tick 1667: angle=0.33707, min=-56.4885, max=30.6413		<- throttle = 0.65
		// tick 259: angle=-59.1568, min=-59.1568, max=27.8469		<- throttle = 0.7		*** CRASH ***
		
		// update min/max angle for stats
		mMinAngle = min(angle, mMinAngle);
		mMaxAngle = max(angle, mMaxAngle);

		// get abs of current angle and previous angle
		const double curAng  = abs(angle);
		const double lastAng = mAngleHistory.back();

		// calculate difference in slippage from last tick
		const double slippage  = curAng - lastAng;
		
		// set initial throttle
		throttle = 1.0;
		if ((track.type == TypeBend) && ((track.radius <= 100.0) || (track.angle >= 45.0)))
			throttle = 0.5;
		
		// apply brake if this is either a straight piece marked as so, or if our angle is increasing
		if ((track.brake == true) && ((track.type == TypeStraight) || (slippage > 0.0)))
			throttle = 0.0;

		const double aslipamt[] = { 5.0,  4.0, 3.0,  2.5, 2.0 };
//		const double aslipamt[] = { 4.0,  3.0, 2.5,  2.0, 1.5 };
		const double throtadj[] = { 0.95, 0.8, 0.75, 0.5, 0.2 };
		
		// taper throttle based on amount of slippage
		const double aslippage = abs(slippage);
		for (int t = 0; t < 5; t++)
		{
			if (aslippage > aslipamt[t])
			{
				throttle /= throtadj[t];
				break;
			}
		}

		// panic if slip angle gets too high
		if ((curAng >= 48.0) && (throttle > 0.1))
			throttle = 0.1;
		else if (curAng >= 52.0)
			throttle = 0.0;
		
		// ensure throttle is clamped between 0 and 1
		throttle = max(0.0, min(1.0, throttle));
		
/*
		// update velocity and estimated distance
		mVelocity += throttle;
		if (mVelocity > MAX_VELOCITY)
			mVelocity = MAX_VELOCITY;
		
		mEstDist += mVelocity;
		
		// calculate actual distance
		if (mCurPiece != mLastPiece)
		{
			mActDist += mTrackPiece[mLastPiece].length;
			mLastPiece = mCurPiece;
		}
		
		const double actdist = mActDist + inDist;
		
		cout << "tick " << mGameTick << ": piece=" << mCurPiece << ", throttle=" << throttle << ", velocity=" << mVelocity << ", estdist=" << mEstDist << ", actdist=" << actdist << ", angle=" << angle << ", min=" << mMinAngle << ", max=" << mMaxAngle << endl;
*/
		
		cout << "tick " << mGameTick << ": piece=" << mCurPiece << ", throttle=" << throttle << ", angle=" << angle << ", min=" << mMinAngle << ", max=" << mMaxAngle << endl;

		// store current angle into history buffer
		mAngleHistory.push_back(curAng);
	}
	else
	{
		cout << "*** error: failed to locate our car in positions update!" << endl;
		throttle = mLastThrottle;
	}

	// store throttle for next iteration
	mLastThrottle = throttle;
	return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const json &data)
{
	cout << "################### Someone crashed" << endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const json &data)
{
	cout << "Race ended" << endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const json &data)
{
	cout << "Error: " << data.to_string() << endl;
	return { make_ping() };
}
