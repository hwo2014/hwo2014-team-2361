#ifndef LOOPED_VECTOR_H
#define LOOPED_VECTOR_H

#include <vector>

template <typename T>
class looped_vector : public std::vector<T>
{
public:
	T &				operator[](int index);
	const T &		operator[](int index) const;
	
	using std::vector<T>::size;
};

template <typename T>
inline T &looped_vector<T>::operator[](int index)
{
	return const_cast<T &>( const_cast<const looped_vector *>(this)->operator[](index) );
}

template <typename T>
inline const T &looped_vector<T>::operator[](int index) const
{
	return std::vector<T>::operator[](index % size());
}

#endif
