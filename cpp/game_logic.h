#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "looped_vector.h"

class game_logic
{
public:
	typedef std::vector<jsoncons::json> msg_vector;

	game_logic();
	msg_vector react(const jsoncons::json &msg);

private:
	typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
	const std::map<std::string, action_fun> action_map;

	double		getLaneOffset(int lane) const;
	double		calculateThrottle(int idx) const;
	double		calculateBendLength(int piece, double offset) const;
	
	msg_vector	on_join(const jsoncons::json &data);
	msg_vector	on_your_car(const jsoncons::json &data);
	msg_vector	on_game_init(const jsoncons::json &data);
	msg_vector	on_game_start(const jsoncons::json &data);
	msg_vector	on_car_positions(const jsoncons::json &data);
	msg_vector	on_crash(const jsoncons::json &data);
	msg_vector	on_game_end(const jsoncons::json &data);
	msg_vector	on_error(const jsoncons::json &data);
	
private:
	enum TrackType
	{
		TypeStraight,
		TypeBend
	};
	
	struct TrackPiece
	{
		TrackType	type;
		bool		brake;
		bool		hasSwitch;
		double		length;
		
		// for bends
		double		radius;
		double		angle;
	};
	
private:
	int				mGameTick, mCurPiece, mLastPiece;
	std::string		mMyCarColor;
	double			mLastThrottle;
	double			mVelocity, mEstDist, mActDist;
	
	std::vector<double>	mAngleHistory;
	double				mMinAngle, mMaxAngle;
	
	std::vector<double>			mLaneDist;
	looped_vector<TrackPiece>	mTrackPiece;
};

#endif
